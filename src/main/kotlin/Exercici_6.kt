import kotlinx.coroutines.*

/*
    * NAME: Erik Vasilyan
    * DATE: 2023-04-15
 */

fun main() {
    println("Start of the race!")

    runBlocking {
        launch { horse(1) }
        launch { horse(2) }
        launch { horse(3) }
        launch { horse(4) }
        launch { horse(5) }
    }

    println("End of the race!")
}

suspend fun horse(id: Int) {
    repeat(4) { currentIteration ->
        val distance = "   ".repeat(currentIteration) + "🐴"
        println("Horse $id: $distance")
        delay((1000L..5000L).random())
    }
    println("Horse $id has finished the race!")
}