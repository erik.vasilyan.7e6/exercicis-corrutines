import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.*
import kotlin.system.exitProcess

/*
    * NAME: Erik Vasilyan
    * DATE: 2023-04-15
 */

fun main() {
    val scanner = Scanner(System.`in`)
    val randomNumber = (1..50).random()

    println("You have 10 seconds to guess the secret number...")

    startTimer()

    do {
        print("\nEnter a number: ")
        val userNumber = scanner.nextInt()
        if (userNumber == randomNumber) println("You got it!")
        else println("Wrong number!")
    } while (userNumber != randomNumber)
}

@OptIn(DelicateCoroutinesApi::class)
fun startTimer() {
    GlobalScope.launch {
        delay(10000)
        println("\nThe time is up!")
        exitProcess(0)
    }
}