import kotlinx.coroutines.*

/*
    * NAME: Erik Vasilyan
    * DATE: 2023-04-13
 */

/*
    * Modifica el codi anterior de manera que el delay del runBlocking tingui un valor més petit que el delay del launch. Quina sortida obtens?
    * La sortida seria el seguent:
    *
    * The main program is started
    * The main program continues
    * Background processing started
    * The main program is finished
 */

@OptIn(DelicateCoroutinesApi::class)
fun main() {
    println("The main program is started")
    GlobalScope.launch {
        println("Background processing started")
        delay(1000)
        println("Background processing finished")
    }
    println("The main program continues")
    runBlocking {
        delay(500)
        println("The main program is finished")
    }
}
