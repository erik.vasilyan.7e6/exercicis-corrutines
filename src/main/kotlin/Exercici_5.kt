import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

/*
    * NAME: Erik Vasilyan
    * DATE: 2023-04-15
 */

/*
    * Executa el següent codi i mira què passa:
    * Primer crida la funció corroutine1() y després quan s'acaba, s'executa la funció corroutine2(), perqué les dues funcions es criden dins de runBlocking
 */

fun main() {
    runBlocking {
        corroutine1()
        corroutine2()
    }
    println("Completed")
}

suspend fun corroutine1() = coroutineScope {
    launch{
        println("Hello World 1.1")
        delay(3000)
        println("Hello World 1.2")
        delay(3000)
        println("Hello World 1.3")
    }
}

suspend fun corroutine2() = coroutineScope {
    launch{
        println("Hello World 2.1")
        delay(2000)
        println("Hello World 2.2")
        delay(2000)
        println("Hello World 2.3")
    }
}
