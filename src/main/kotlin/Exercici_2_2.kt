import kotlinx.coroutines.*

/*
    * NAME: Erik Vasilyan
    * DATE: 2023-04-13
 */

fun main() {
    runBlocking {
        launch {
            printHelloWorldP2(10)
        }
    }
    println("Finished")
}

suspend fun printHelloWorldP2(times: Int) {
    repeat(times) {
        println("Hello World!")
        delay(100)
    }
}
