import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

/*
    * NAME: Erik Vasilyan
    * DATE: 2023-04-15
 */

fun main() {
    runBlocking {
        launch { corroutine3() }
        launch { corroutine4() }
    }
    println("Completed")
}

suspend fun corroutine3() = coroutineScope {
    launch{
        println("Hello World 1.1")
        delay(3000)
        println("Hello World 1.2")
        delay(3000)
        println("Hello World 1.3")
    }
}

suspend fun corroutine4() = coroutineScope {
    launch{
        println("Hello World 2.1")
        delay(2000)
        println("Hello World 2.2")
        delay(2000)
        println("Hello World 2.3")
    }
}
