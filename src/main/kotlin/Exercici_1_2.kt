import kotlinx.coroutines.*

/*
    * NAME: Erik Vasilyan
    * DATE: 2023-04-13
 */

@OptIn(DelicateCoroutinesApi::class)
fun main() {
    println("The main program is started")
    GlobalScope.launch {
        doBackground1()
    }
    println("The main program continues")
    runBlocking {
        delay(500)
        println("The main program is finished")
    }
}

suspend fun doBackground1() {
    println("Background processing started")
    delay(1000)
    println("Background processing finished")
}
