import kotlinx.coroutines.*

/*
    * NAME: Erik Vasilyan
    * DATE: 2023-04-13
 */

fun main() = runBlocking {
    println("The main program is started")
    withContext(Dispatchers.Default) {
        doBackground2()
    }
    println("The main program continues")
    delay(500)
    println("The main program is finished")
}

suspend fun doBackground2() {
    println("Background processing started")
    delay(1000)
    println("Background processing finished")
}

