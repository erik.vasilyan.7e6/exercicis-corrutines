import kotlinx.coroutines.*

/*
    * NAME: Erik Vasilyan
    * DATE: 2023-04-13
 */

fun main() {
    printHelloWorldP1(10)
    println("Finished")
}

fun printHelloWorldP1(times: Int) {
    runBlocking {
        launch {
            repeat(times) {
                println("Hello World!")
                delay(100)
            }
        }
    }
}