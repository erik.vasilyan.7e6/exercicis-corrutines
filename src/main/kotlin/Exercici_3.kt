import kotlinx.coroutines.*
import java.io.File
import kotlin.system.measureTimeMillis

/*
    * NAME: Erik Vasilyan
    * DATE: 2023-04-15
 */

fun main() {
    val song = File("song.txt").readLines()

    val time = measureTimeMillis {
        for (line in song) {
            if (line == "") {
                runBlocking {
                    delay(3000)
                }
            }
            println(line)
        }
    }

    println("Time: $time millis")
}